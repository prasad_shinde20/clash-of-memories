"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const functions = require("firebase-functions");
// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
exports.helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});
exports.myFunctionName = functions.firestore
    .document('users/abcd').onWrite((change, context) => {
    // ... Your code here
    console.log("Trigger created");
    // let FCM = require('fcm-node');
    // let serverKey = 'AAAASQ4H5tQ:APA91bENvCIXpk-OV7mBECqXFJ75bf4pa4j3RQB8jHkPOCB0V0SgD1epD0d4FHQlmEFvngiW4Lu3of0zjPcyUCW8B9omcawqPLRwEB8UIUGj0v-z8etcU9Ma-lnuWG-KqKX-w6CwPkN7'; //put your server key here
    // let fcm = new FCM(serverKey);
    // let message = { //this may lety according to the message type (single recipient, multicast, topic, et cetera)
    //     to: 'cNdxUmaiLKA:APA91bGi6eQm3N_-l3Ago5W-xRPToCsxrogCZ8QUpze2PAziWElhKW_0byL77zn3I9WKcGqvN0W4T_SjTubRcc1_n9G_UNUBs_HLre1Nezn-DpK-bGmH08xFG8tA24_oG6TQZAKbixTl', 
    //     data: {  //you can send only notification or only data(or include both)
    //         my_key: 'my value',
    //         my_another_key: 'my another value'
    //     }
    // };
});
exports.addMessage = functions.https.onCall((data, context) => {
    // ...
    console.log("add a message");
});
// This registration token comes from the client FCM SDKs.
// let registrationToken = 'cNdxUmaiLKA:APA91bGi6eQm3N_-l3Ago5W-xRPToCsxrogCZ8QUpze2PAziWElhKW_0byL77zn3I9WKcGqvN0W4T_SjTubRcc1_n9G_UNUBs_HLre1Nezn-DpK-bGmH08xFG8tA24_oG6TQZAKbixTl';
//# sourceMappingURL=index.js.map