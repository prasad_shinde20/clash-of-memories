const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);


exports.playAGame = functions.https.onCall((data, context) =>
{
    const userId = data.uid;
    var db = admin.firestore();
    var firstNonStartedGameId = "-1";
    return db.collection("games").where("joinCount", "==", 1).limit(1)
        .get()
        .then(snapshot => {
            snapshot.forEach(doc => {
                var newelement = {
                    "id": doc.id,
                    "status": doc.data().status,
                    "joinCount": doc.data().joinCount 
                }
                if (firstNonStartedGameId === "-1" && doc.data().joinCount === 1) {
                    firstNonStartedGameId = doc.id;
                }
            });
            return {"firstNonStartedGameId": firstNonStartedGameId};
        })
        .then (snapshotResult => {            
            if (firstNonStartedGameId === "-1") {
                return createGameForUser(userId);
            }
            else {
                return updateGameState(firstNonStartedGameId, userId);
            }
        }).catch(reason => {
            console.log("Inside catch with reason : " + reason);
            return { "success": false };
        });
         
});

function createGameForUser(userId)
{
    var db = admin.firestore();
    var timestamp = Date.now();
    return db.collection("games").add({ 
        "status": "not_started",
        "joinCount": 1,
        "created" : timestamp
    })
    .then(docRef => {
        console.log("document id : "+ docRef.id);
        return createBoard(docRef.id);
    })
    .then ( createBoardResult => {
        return joinGameForUser(userId, createBoardResult);
    })
    .catch(error => {
        console.log("Error : " + error);
        return  { "success": false};
    });
}

function joinGameForUser(userId, response)
{
    const gameId = response.gameId;
    var db = admin.firestore();
    var timestamp = Date.now();
    return db.collection("games").doc(gameId).collection("users").doc(userId).set({
            "gameId" : gameId,
            "joinTime" : timestamp,
            "matchedCount" : 0
        })
        .then(addUserResult => {
            return response;
        })
        .catch(error => {
            console.log("Error : " + error);
            return  { "success": false};
        });
}

function updateGameState(firstNonStartedGameId, userId)
{
    var db = admin.firestore();
    return joinGameForUser(userId, { "gameId" : firstNonStartedGameId })
    .then ( joinGameForUserResult => {
        return db.collection("games").doc(firstNonStartedGameId).update({
            "status": "ready_to_start",
            "joinCount": 2
        })
        .then (updateGameStateResult => {
            return  { "gameId" : firstNonStartedGameId };
        });
    })
    .catch(error => {
        console.log("Error : " + error);
        return  { "success": false};
    });
}

function createBoard(gameId) {
    var db = admin.firestore();
    var i=0;
    var tilesIndices = [];
    var emojiMap = [];
    for(i=0; i<16; i++) {
        tilesIndices.push(i);
    }
    for(i=0; i<8;i++) {
        var j=0;
        for (j=0;j<2; j++) {
            var random = Math.floor((Math.random() * (tilesIndices.length-1)) + 0);
            emojiMap[tilesIndices[random]] = i;
            tilesIndices.splice(random,1);
        }
    }
    db.collection("games").doc(gameId).update({
        "initialBoardLayout" : emojiMap
    });
    return { "gameId" : gameId };
}


exports.onGameUpdate = functions.firestore
    .document('games/{gameId}')
    .onUpdate((change, context) => {
        var gameId = JSON.stringify(change.before.data().gameId);
        return {};
});

exports.onGameUpdateUser = functions.firestore
    .document('users/{userId}')
    .onUpdate((change, context) => {
        var gameId = JSON.stringify(change.before.data()).userId;      
        return {};
});