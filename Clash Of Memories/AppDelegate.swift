//
//  AppDelegate.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var firestore: Firestore!
    var functions: Functions!
    var currentGame: CurrentGame!
    var user: User?
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url, sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        firestore = Firestore.firestore()
        functions = Functions.functions()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if Auth.auth().currentUser == nil {
            window?.rootViewController = RootVC(with: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllers.LoginVC.rawValue))
        } else {
            window?.rootViewController = RootVC(with: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: ViewControllers.MainVC.rawValue))
            firestore.collection("users").document((Auth.auth().currentUser?.uid)!).getDocument { (document, error) in
                if let error = error {
                    print(error)
                    return
                }
                
                if let data = document?.data() {
                    self.user = User((Auth.auth().currentUser?.uid)!, data)
                } else {
                    print("Document does not exist in cache")
                }
            }
        }
        window?.makeKeyAndVisible()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
}

extension AppDelegate {
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    var rootViewController: RootVC {
        return window!.rootViewController as! RootVC
    }
}

extension Notification.Name {
    static let didUserSet = Notification.Name("didUserSet")
}

extension Date {
    var millisecondsSince1970: String {
        return String((self.timeIntervalSince1970 * 1000.0).rounded())
    }
}
