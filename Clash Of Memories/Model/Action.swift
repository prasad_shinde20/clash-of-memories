//
//  Action.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 30/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

class Action {
    var type: ActionType!
    var result: ResultOfTap!
    var timestamp: String = Date().millisecondsSince1970
    
    init(type: ActionType, result: ResultOfTap) {
        self.type = type
        self.result = result
    }
    
    func dic() -> [String: Any] {
        return ["type": type.rawValue,
                "result": result.rawValue,
                "timestamp": timestamp]
    }
}

enum ActionType: String {
    case open
    case close
}

enum ResultOfTap: String {
    case success
    case fail
    case noResult
}
