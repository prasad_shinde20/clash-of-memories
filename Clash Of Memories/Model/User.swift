//
//  User.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import Foundation
import FirebaseFirestore

class User {
    var uId: String?
    var name: String?
    var profileImg: UIImage? {
        didSet {
            NotificationCenter.default.post(name: .didUserSet, object: nil)
        }
    }
    
    init(_ uId: String, _ doc: [String: Any]) {
        self.uId = uId
        self.name = (doc["name"] as! String)
        let urlString = doc["profileImgUrl"] as! String
        let url = URL(string: urlString)
        
        DispatchQueue.main.async {
            let data = NSData(contentsOf: url!)
            self.profileImg = UIImage(data: data! as Data)
        }
    }
}
