//
//  CurrentGame.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 30/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

class CurrentGame {
    var gameId: String
    var status: String
    var joinCount: Int
    var initialBoardLayout: [Int]
    var opponentUid: String?
    var opponentMatchCount: Int = 0
    var myMatchCount: Int = 0
    
    init(_ id: String, _ doc: [String: Any]) {
        self.gameId = id
        self.status = doc["status"] as! String
        self.joinCount = doc["joinCount"] as! Int
        self.initialBoardLayout = doc["initialBoardLayout"] as! [Int]
    }
}
