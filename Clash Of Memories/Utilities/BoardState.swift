//
//  BoardState.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import Foundation

enum BoardState {
    case allClosed
    case oneOpen(Int)
    case twoOpen(Int, Int, Bool)
}
