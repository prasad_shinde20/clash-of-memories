//
//  RootVC.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

class RootVC: UIViewController {
    
    private var current: UIViewController!
    
    init(with viewController: UIViewController) {
        current = viewController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupChildVC(current)
    }
    
    func show(_ viewControllerType: ViewControllers) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerType.rawValue)
        setupChildVC(viewController)
        setupCurrentVC(with: viewController)
    }
    
    private func setupChildVC(_ viewController: UIViewController) {
        addChild(viewController)
        viewController.view.frame = view.bounds
        view.addSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
    
    private func setupCurrentVC(with viewController: UIViewController) {
        current.willMove(toParent: nil)
        current.view.removeFromSuperview()
        current.removeFromParent()
        current = viewController
    }
}

