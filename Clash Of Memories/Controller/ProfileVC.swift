//
//  ProfileVC.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 30/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    @IBAction func goBack() {
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var profileImg: PictureImageView! {
        didSet {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goBack))
            profileImg.addGestureRecognizer(tapGestureRecognizer)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let user = AppDelegate.shared.user,
            let img = user.profileImg{
            profileImg.image = img
            username.text = user.name
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
