//
//  LoginVC.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FirebaseFirestore

class LoginVC: UIViewController, GIDSignInUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
}

extension LoginVC: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print(error)
                return
            }
           let imgUrl = user.profile.imageURL(withDimension: 130)?.absoluteString
            AppDelegate.shared.firestore.collection("users").document((Auth.auth().currentUser?.uid)!).setData([
                "name": user.profile.name as Any,
                "profileImgUrl": imgUrl as Any], merge: true)
            
            AppDelegate.shared.rootViewController.show(.MainVC)
        }
    }
}

