//
//  LobbyVC.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit
import FirebaseAuth
import GoogleSignIn

class LobbyVC: UIViewController {
    
    @IBOutlet weak var profileImg: PictureImageView! {
        didSet {
            let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(goToProfileVC))
            profileImg.addGestureRecognizer(tapGestureRecogniser)
        }
    }
    
    @IBOutlet weak var usernameLbl: UILabel! {
        didSet {
            let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: #selector(goToProfileVC))
            usernameLbl.addGestureRecognizer(tapGestureRecogniser)
        }
    }
    
    @IBOutlet weak var waitIndicator: UIActivityIndicatorView! {
        didSet {
            waitIndicator!.frame = CGRect(x: (profileImg.frame.width - 20)/2, y: (profileImg.frame.height - 20)/2, width: 20, height: 20)
            waitIndicator.startAnimating()
        }
    }
    
    @IBAction func startMultiplayerGameBtnTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "LobbyToGameBoard", sender: self)
    }
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(userIsSet), name: .didUserSet, object: nil)
        super.viewDidLoad()
        usernameLbl.backgroundColor = #colorLiteral(red: 0.9019607843, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
    }
    @IBAction func logoutBtnTapped() {
        do {
            try Auth.auth().signOut()
            GIDSignIn.sharedInstance()?.signOut()
        }
        catch let error {
            print(error)
        }
        AppDelegate.shared.rootViewController.show(.LoginVC)
    }
    
    @objc func goToProfileVC() {
        performSegue(withIdentifier: "LobbyToProfile", sender: self)
    }
    
    @objc func userIsSet() {
        self.waitIndicator.stopAnimating()
        self.waitIndicator.removeFromSuperview()
        self.profileImg.image = AppDelegate.shared.user!.profileImg
        self.usernameLbl.text = AppDelegate.shared.user!.name
        usernameLbl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
}



