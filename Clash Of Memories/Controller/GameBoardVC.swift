//
//  GameBoardVC
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit
import FirebaseFunctions
import FirebaseFirestore
import FirebaseAuth
import GoogleSignIn

class GameBoardVC: UIViewController {
    
    var myScore: Int! = 0 {
        didSet {
            myScoreLbl.text = String(myScore)
        }
    }
    var oppScore: Int! = 0 {
        didSet {
            oppScoreLbl.text = String(oppScore)
        }
    }
    var unlock: Bool! = true
    var timer: Timer!
    var state: BoardState!
    var actions = [[String: Any]]()
    var listener: ListenerRegistration!
    var listener2: ListenerRegistration!
    var waitingView: WaitingView!
    var gameStartTimestamp: String!
    var isFirstTilePressed: Bool! = false
    var curGameDocRef: DocumentReference!
    var emojis = ["⚽️","🏀","🏈","⚾️","🎾","🏐","🏉","🎱","🔴","🔵"]
    var emojiMap = [String](repeating: "", count: 16)
    var tilesIndices = [Int]()
    var tilesVisible = [Bool](repeating: true, count: 16)
    @IBOutlet var tiles: [Tile]!
    @IBOutlet weak var myScoreLbl: UILabel!
    @IBOutlet weak var oppScoreLbl: UILabel!
    
    var twoSuccessfulFlips: Bool! = true {
        didSet {
            if twoSuccessfulFlips {
                AppDelegate.shared.currentGame.myMatchCount += 1
                curGameDocRef.collection("users").document(AppDelegate.shared.user!.uId!).updateData(["matchedCount": AppDelegate.shared.currentGame.myMatchCount])
                oppScore -= 1
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.shared.functions.httpsCallable("playAGame").call(["uid": Auth.auth().currentUser!.uid]) { (result, error) in
            if let error = error as NSError? {
                print(error)
                return
            }
            if let gameId = (result?.data as? [String: Any])?["gameId"] as? String {
                self.curGameDocRef = AppDelegate.shared.firestore.collection("games").document(gameId)
                self.curGameDocRef.getDocument(completion: { (document, error) in
                    if let error = error {
                        print(error)
                        return
                    }
                    
                    AppDelegate.shared.currentGame = CurrentGame(gameId,document!.data()!)
                    for i in 0...15 {
                        self.emojiMap[i] = self.emojis[AppDelegate.shared.currentGame.initialBoardLayout[i]]
                    }
                    print("emoji \(self.emojiMap)")
                    if AppDelegate.shared.currentGame.joinCount == 2 {
                        self.startGame()
                    } else {
                        self.listener  = self.curGameDocRef.addSnapshotListener({ (document, error) in
                            if let error = error {
                                print(error)
                            }
                            AppDelegate.shared.currentGame = CurrentGame(document!.documentID, document!.data()!)
                            if AppDelegate.shared.currentGame.joinCount == 2 {
                                self.startGame()
                            }
                        })
                    }
                })
            }
        }
        waitingView = WaitingView(frame: view.frame)
        waitingView.setProperties()
        view.addSubview(waitingView)
        
        state = .allClosed
    }
    
    func startGame() {
        if let listener = listener {
            listener.remove()
        }
        curGameDocRef.collection("users").getDocuments { (snapshot, error) in
            if let error = error {
                print(error)
                return
            }
            self.curGameDocRef.addSnapshotListener({ (snapshot, error) in
                if let error = error {
                    print(error)
                    return
                }
                if let data = snapshot?.data() {
                    if data["winner"] != nil {
                        self.unlock = false
                        if data["winner"] as? String == AppDelegate.shared.currentGame.opponentUid {
                            self.finishTheGame(result: .lose)
                        } else {
                            self.finishTheGame(result: .win)
                        }
                    }
                }
            })
            for document in snapshot!.documents {
                if document.documentID != AppDelegate.shared.user!.uId! {
                    AppDelegate.shared.currentGame.opponentUid = document.documentID
                    self.listener = self.curGameDocRef.collection("users").document(AppDelegate.shared.currentGame.opponentUid!).addSnapshotListener({ (document, error) in
                        if let error = error {
                            print(error)
                            return
                        }
                        
                        if let data = document?.data() {
                            
                            if data["score"] != nil {
                                self.oppScore = data["score"] as? Int
                            }
                            
                            
                            if AppDelegate.shared.currentGame.opponentMatchCount < data["matchedCount"] as! Int {
                                AppDelegate.shared.currentGame.opponentMatchCount = data["matchedCount"] as! Int
                                self.myScore -= 1
                                self.closeAPair()
                            }
                            
                            
                        }
                    })
                }
            }
        }
        
        waitingView.removeFromSuperview()
    }
    
    @IBAction func quitBtnPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Quit", message: "Are you sure you want to quit?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.curGameDocRef.updateData(["winner": AppDelegate.shared.currentGame.opponentUid!])
            self.navigationController?.popViewController(animated: true)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func tilePressed(_ curTile: Tile) {
        if !unlock {
            return
        }
        if !isFirstTilePressed {
            gameStartTimestamp = Date().millisecondsSince1970
            isFirstTilePressed = true
        }
        changeState(with: curTile)
    }
    
    func closeAllOpenTiles() {
        for i in 1..<tiles.count {
            if tilesVisible[i] {
                tiles[i].close()
            }
        }
        state = .allClosed
    }
    func changeState(with curTile: Tile) {
        
        let curTileIndex = tiles.index(of: curTile)!
        print("curTileIndex = \(curTileIndex)")
        if let timer = timer {
            timer.fire()
        }
        switch state! {
            
        case .allClosed:
            openTile(tile: curTile)
            actions.append(Action(type: .open, result: .noResult).dic())
            state = .oneOpen(curTileIndex)
            print("abc \(curTile)")
            
        case .oneOpen(let index_1):
            if index_1 == curTileIndex {
                closeTiles(curTile, nil)
                actions.append(Action(type: .close, result: .noResult).dic())
                state = .allClosed
                return
            }
            openTile(tile: curTile)
            state = .twoOpen(index_1, curTileIndex, matchIfPossible(previous: tiles[index_1], current: curTile))
            
        case .twoOpen(let index_1, let index_2, let result):
            
            if result {
                removeTiles(tiles[index_1], tiles[index_2])
                hideTiles(tiles[index_1], tiles[index_2])
            } else {
                closeTiles(tiles[index_1], tiles[index_2])
            }
            
            if index_1 == curTileIndex || index_2 == curTileIndex {
                actions.append(Action(type: .close, result: .noResult).dic())
                state = .allClosed
            } else {
                openTile(tile: curTile)
                actions.append(Action(type: .open, result: .noResult).dic())
                state = .oneOpen(curTileIndex)
            }
        }
    }
    
    func openTile(tile: Tile) {
        tile.open(with: emojiMap[tiles.index(of: tile)!])
    }
    
    func matchIfPossible(previous: Tile, current: Tile) -> Bool{
        if previous.titleLabel?.text == current.titleLabel?.text {
            myScore += 1
            curGameDocRef.collection("users").document(AppDelegate.shared.user!.uId!).updateData(["score": myScore])
            removeTiles(previous, current)
            twoSuccessfulFlips.toggle()
            actions.append(Action(type: .open, result: .success).dic())
            timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false) { (timer) in
                self.hideTiles(previous, current)
                self.state = .allClosed
            }
            return true
        } else {
            actions.append(Action(type: .open, result: .fail).dic())
            timer = Timer.scheduledTimer(withTimeInterval: 0.6, repeats: false) { (timer) in
                self.closeTiles(previous, current)
                self.state = .allClosed
            }
            return false
        }
    }
    
    
    func closeTiles(_ tile_1: Tile?, _ tile_2: Tile?) {
        tile_1?.close()
        tile_2?.close()
    }
    
    func removeTiles(_ tile_1: Tile, _ tile_2: Tile) {
        tile_1.isUserInteractionEnabled = false
        tile_2.isUserInteractionEnabled = false
        tile_1.isEnabled = false
        tile_2.isEnabled = false
        tilesVisible[tiles.index(of: tile_1)!] = false
        tilesVisible[tiles.index(of: tile_2)!] = false
    }
    
    func hideTiles(_ tile_1: Tile, _ tile_2: Tile) {
        tile_1.hide()
        tile_2.hide()
        for tile in tilesVisible {
            if tile {
                return
            }
        }
        curGameDocRef.updateData(["winner": Auth.auth().currentUser!.uid])
        finishTheGame(result: .win)
    }
    
    func finishTheGame(result: MatchResult) {
        print("game over")
        switch result {
        case .lose:
            let alert = UIAlertController(title: "You lost!", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
            
        case .win:
            let alert = UIAlertController(title: "You won!", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                self.navigationController?.popViewController(animated: true)
            }))
            present(alert, animated: true, completion: nil)
            
        }
//        if actions.count > 1 {
//            for i in (1..<actions.count).reversed() {
//                actions[i]["timestamp"] = String(format: "%.2f", Double(actions[i]["timestamp"] as! String)! - Double(actions[i-1]["timestamp"] as! String)!)
//            }
//            actions[0]["timestamp"] = 0
//        }
     //   print(actions)
    }
    
    func closeAPair() {
        unlock = false
        if let timer = timer {
            timer.fire()
        }
        
        var vacantSpaces = [Int]()
        var filledSpaces = [Int]()
        var newTiles = [Int]()
        var tilesToBeShuffledWithNewTiles = [Int]()
        for i in 0..<tilesVisible.count {
            if tilesVisible[i] {
                filledSpaces.append(i)
            } else {
                vacantSpaces.append(i)
            }
        }
        if vacantSpaces.count == 0 {
            unlock = true
            return
        }
        
        var randomisedArr_1 = getRandomisedArray(ofLength: vacantSpaces.count)
        var randomisedArr_2 = getRandomisedArray(ofLength: filledSpaces.count)
        
        for i in 0...1 {
            newTiles.append(vacantSpaces[randomisedArr_1[i]])
            tilesToBeShuffledWithNewTiles.append(filledSpaces[randomisedArr_2[i]])
            emojiMap.swapAt(newTiles[i], tilesToBeShuffledWithNewTiles[i])
            tiles[newTiles[i]].show()
            tilesVisible[newTiles[i]] = true
        }
        unlock = true
        print("emoji \(emojiMap)")
    }
    
    func getRandomisedArray(ofLength length: Int) -> [Int] {
        var temp = [Int]()
        var randomisedArr = [Int]()
        for i in 0..<length {
            temp.append(i)
        }
        
        for _ in 0..<length {
            let random = Int(arc4random_uniform(UInt32(temp.count)))
            randomisedArr.append(temp[random])
            temp.remove(at: random)
        }
        return randomisedArr
    }
}


enum MatchResult {
    case win
    case lose
}

