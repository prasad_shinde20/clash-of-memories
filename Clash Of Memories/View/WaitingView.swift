//
//  WaitingView.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

class WaitingView: UIView {
    
    var waitIndicator: UIActivityIndicatorView! {
        didSet {
            waitIndicator!.frame = CGRect(x: (innerView.frame.width - 20)/2, y: 20, width: 20, height: 20)
            waitIndicator.startAnimating()
        }
    }
    
    var waitLbl: UILabel! {
        didSet {
            waitLbl.frame = CGRect(x: (innerView.frame.width - 200)/2, y: waitIndicator.frame.maxY, width: 200, height: 50)
            waitLbl.text = "Finding a player for you..."
        }
    }
    
    var innerView: UIView! {
        didSet {
            innerView.frame = CGRect(x: (frame.maxX - 220)/2, y: (frame.maxY - 100)/2, width: 220, height: 100)
            innerView.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            innerView.layer.cornerRadius = 10
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func setProperties() {
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.7046500428)
        innerView = UIView()
        waitIndicator = UIActivityIndicatorView(style: .gray)
        innerView.addSubview(waitIndicator)
        waitLbl = UILabel()
        innerView.addSubview(waitLbl)
        addSubview(innerView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
