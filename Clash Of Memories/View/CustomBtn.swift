//
//  LobbyBtn.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 30/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

@IBDesignable class CustomBtn: UIButton {
    @IBInspectable var cornerRadius: Double = 0.0 {
        didSet {
            self.layer.cornerRadius = CGFloat(cornerRadius)
        }
    }
}
