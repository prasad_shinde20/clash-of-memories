//
//  Tile.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 29/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

@IBDesignable class Tile: UIButton {
    
    override func awakeFromNib() {
        
    }
    
    @IBInspectable var cornerRadius: Double = 0.0 {
        didSet {
            self.layer.cornerRadius = CGFloat(cornerRadius)
        }
    }
    
    func open(with emoji: String) {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.setTitle(emoji, for: .normal)
        }, completion: nil)
    }
    
    func close() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            self.setTitle("", for: .normal)
        }, completion: nil)
    }
    
    func hide() {
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.setTitle("", for: .normal)
        }, completion: nil)
    }
    
    func show() {
        self.isUserInteractionEnabled = true
        self.isEnabled = true
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            self.setTitle("", for: .normal)
        }, completion: nil)
    }
}
