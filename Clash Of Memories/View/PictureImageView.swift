//
//  PictureImageView.swift
//  Clash Of Memories
//
//  Created by prasad-shinde on 30/03/19.
//  Copyright © 2019 prasad-shinde. All rights reserved.
//

import UIKit

class PictureImageView: UIImageView {
    override func layoutSubviews() {
        layer.cornerRadius = frame.size.width/2
        layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        layer.borderWidth = 2
    }
}

